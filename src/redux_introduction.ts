import { combineReducers, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'

const initialState = 0
const store = createStore(
  combineReducers({
    calculator: calculatorReducer,
  }),
  composeWithDevTools(),
)

store.subscribe(() => {
  console.log('Store =', store.getState())
})

store.dispatch({ type: 'ADD', payload: 5 })
store.dispatch({ type: 'ADD', payload: 2 })

setTimeout(() => {
  store.dispatch({ type: 'REMOVE', payload: 4 })
}, 2000)

setTimeout(() => {
  store.dispatch({ type: 'RESET' })
}, 5000)

function calculatorReducer(state = initialState, action: any) {
  switch (action.type) {
    case 'ADD':
      return state + action.payload
    case 'REMOVE':
      return state - action.payload
    case 'RESET':
      return initialState
  }
  return state
}

const ADD = 'add'
const REMOVE = 'remove'
const RESET = 'reset'

const add = (value: number) => ({ payload: value, type: ADD }) // Action Creator
const remove = (value: number) => ({ payload: value, type: REMOVE })
const reset = () => ({ type: RESET })

let values = [
  { payload: 1, type: ADD }, // Action
  remove(3),
  add(5),
  reset(),
]

values.reduce(reducer, 0)

function reducer(state: any, action: any) {
  switch (action.type) {
    case ADD:
      return state + action.payload
    case REMOVE:
      return state - action.payload
    case RESET:
      return 0
  }

  return state
}
