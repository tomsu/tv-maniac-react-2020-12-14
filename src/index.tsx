import React from 'react'
import ReactDOM from 'react-dom'
import './index.scss'
import App from './App'
import reportWebVitals from './reportWebVitals'
import { store } from './store'
import { calculatorStore } from './calculator.store'

store.subscribe(() => {
  console.log('Store = ', store.getState())
})

store.dispatch(calculatorStore.actions.add(2))
store.dispatch(calculatorStore.actions.add(2))
store.dispatch(calculatorStore.actions.reset())

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root'),
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
