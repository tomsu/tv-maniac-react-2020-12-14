import { FC, useContext } from 'react'
import { SettingsContext } from './SettingsContext'
import { Columns } from './settings.models'

const columns: Columns[] = [1, 2, 3, 4]

export const Settings: FC = () => {
  const settings = useContext(SettingsContext)

  return (
    <>
      <h1 className="h3 mb-4">Settings</h1>

      <div className="w-25">
        <label>How many columns?</label>
        <select
          className="form-control"
          value={settings.columns}
          onChange={(event) =>
            settings.setColumns(+event.target.value as Columns)
          }
        >
          {columns.map((col) => (
            <option key={col}>{col}</option>
          ))}
        </select>
      </div>
    </>
  )
}
