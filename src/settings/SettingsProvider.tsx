import React, { FC, useState } from 'react'
import { SettingsContext } from './SettingsContext'
import { Columns } from './settings.models'

type Props = {
  columns?: Columns
}

export const SettingsProvider: FC<Props> = ({
  children,
  columns: cols = 1,
}) => {
  const [columns, setColumns] = useState<Columns>(cols)

  return (
    <SettingsContext.Provider value={{ columns, setColumns }}>
      {children}
    </SettingsContext.Provider>
  )
}
