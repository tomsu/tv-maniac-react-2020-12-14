import React from 'react'
import { Columns } from './settings.models'

export const SettingsContext = React.createContext({
  columns: 1 as Columns,
  setColumns: (value: Columns) => {},
})

SettingsContext.displayName = 'Settings'
