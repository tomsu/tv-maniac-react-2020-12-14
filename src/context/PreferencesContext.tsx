import React from 'react'

export const PreferencesContext = React.createContext({
  initialQuery: '',
  itemsPerPage: 0,
  setInitialQuery: (query: string) => {}
})

PreferencesContext.displayName = 'Preferences'
