import styled from '@emotion/styled'

type Props = {
  value: number
}

export const Badge = styled.span<Props>`
  display: inline-block;
  border-radius: 20px;
  width: ${(props) => props.value * 10}px;
  background: ${(props) => (props.value > 10 ? 'red' : '#bada55')};
  text-align: center;
  &:hover {}
  small {
    text-decoration: underline;
  }
`

export const BigBadge = styled(Badge)`
  height: 30px;
  text-align: right;
`

export const BadgeHeader = styled(Badge)`
  font-weight: bold
`.withComponent('h4')
