import { ComponentType, useEffect } from 'react'

export const withTitle = (
  PageComponent: ComponentType,
  title: string,
) => () => {
  useEffect(() => {
    document.title = `TV :: ${title}`

    return () => {
      document.title = 'TV Maniac'
    }
  }, [])

  return (
    <>
      <PageComponent />
    </>
  )
}
