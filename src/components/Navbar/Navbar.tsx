import { FC } from 'react'
import { NavLink } from 'react-router-dom'

export const Navbar: FC = () => {
  return (
    <nav className="navbar navbar-expand navbar-dark bg-dark">
      <div className="container">
        <NavLink className="navbar-brand" to="/">
          TV Maniac
        </NavLink>
        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div className="navbar-nav">
            <NavLink className="nav-link" to="/" exact>
              Home
            </NavLink>
            <NavLink className="nav-link" to="/tv">
              Shows
            </NavLink>
            <NavLink className="nav-link" to="/settings">
              Settings
            </NavLink>
            <NavLink className="nav-link" to="/contact">
              Contact
            </NavLink>
          </div>
        </div>
      </div>
    </nav>
  )
}
