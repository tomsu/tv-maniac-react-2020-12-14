import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import { SearchForm } from './SearchForm'

describe('SearchForm', () => {
  it('has a Search button', async () => {
    const utils = render(<SearchForm onSearch={() => {}} />)
    const button = await utils.findByText('Search')
    expect(button).toBeDefined()
  })

  it('sets a Search button as disabled if the query is empty', () => {
    const utils = render(<SearchForm onSearch={() => {}} initialValue="" />)
    const button = utils.getByText('Search')
    expect(button).toBeDisabled()
  })

  it('sets a Search button as enabled if the query is not empty', () => {
    const utils = render(<SearchForm onSearch={() => {}} initialValue="abcd" />)
    const button = utils.getByText('Search')
    expect(button).toBeEnabled()
  })

  it('enables Search button when user types into query input', () => {
    const utils = render(<SearchForm onSearch={() => {}} initialValue="" />)
    const button = utils.getByText('Search')
    const searchInput = utils.getByTitle('query')

    fireEvent.change(searchInput, { target: { value: 'abcdef' } })

    expect(button).toBeEnabled()
  })
})
