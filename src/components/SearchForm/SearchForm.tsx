import React, { FC, useContext, useEffect, useRef, useState } from 'react'
import { PreferencesContext } from '../../context/PreferencesContext'
import { useDebounce } from '../../hooks/useDebounce'

type Props = {
  initialValue?: string
  onSearch: (query: string) => void
}

// 1. Create 'Settings' page, add to menu
// 2. Create <select> element with values: 1, 2, 3, 4
// 3. Create SettingsContext
// 4. Store selected number in SettingsContext
// 5. use the context value in Search.tsx


export const SearchForm: FC<Props> = ({ initialValue = '', onSearch }) => {
  const { itemsPerPage, initialQuery, setInitialQuery } = useContext(
    PreferencesContext,
  )

  const [query, setQuery] = useState(initialQuery)
  const [clickCt, setClickCt] = useState(0)
  const dValue = useDebounce(query, 2000)

  const inputRef = useRef<HTMLInputElement>(null)

  useEffect(() => {
    console.count('EFFECT')
    console.log({ itemsPerPage })
    // setTimeout(() => setInitialQuery('batman'), 2000)
    // const interval = setInterval(() => {
    //   console.log('tick')
    // }, 1000)

    inputRef.current?.focus()

    return () => {
      // clearInterval(interval)
      console.count('EFFECT RETURN FUNCTION')
    }
  }, [])

  useEffect(() => {
    onSearch(dValue)
  }, [dValue])

  const search = (query: string) => {
    // our custom logic
  }

  return (
    <form className="input-group mb-4">
      <input
        ref={inputRef}
        type="search"
        className="form-control"
        value={query}
        onChange={(event) => setQuery(event.target.value)}
      />
      <div className="input-group-append">
        <button
          type="submit"
          onClick={() => {
            onSearch(query)
            setClickCt((v) => v + 1)
          }}
          className="btn btn-primary"
        >
          Search ({clickCt})
        </button>
      </div>
      <p className="w-100 my-0">query: {query}</p>
      <p className="w-100 my-0">debounced: {dValue}</p>
    </form>
  )
}

// export class SearchForm extends React.Component<Props, State> {
//   state: State = {
//     query: this.props.initialValue || '',
//   }
//
//   render() {
//     console.count('SearchForm: render')
//
//     const { query } = this.state
//     const { onSearch } = this.props
//
//     return (
//       <form className="input-group mb-4">
//         <input
//           type="search"
//           className="form-control"
//           value={query}
//           onChange={(event) => this.setState({ query: event.target.value })}
//         />
//         <div className="input-group-append">
//           <button
//             type="submit"
//             onClick={() => onSearch(query)}
//             className="btn btn-primary"
//           >
//             Search
//           </button>
//         </div>
//       </form>
//     )
//   }
// }
