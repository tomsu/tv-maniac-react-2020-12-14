import { createSlice, PayloadAction } from '@reduxjs/toolkit'

const initialState = {
  result: 0,
  prev: 0,
}

export const calculatorStore = createSlice({
  name: 'calculator',
  initialState,
  reducers: {
    add(state, { payload }: PayloadAction<number>) {
      state.prev = state.result
      state.result = state.result + payload
    },
    remove(state, { payload }: PayloadAction<number>) {
      state.prev = state.result
      state.result = state.result - payload
    },
    reset() {
      return initialState
    },
  },
})
