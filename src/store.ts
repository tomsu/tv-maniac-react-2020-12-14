import { configureStore, createSelector } from '@reduxjs/toolkit'
import { calculatorStore } from './calculator.store'
import { bookmarksSlice } from './bookmarks/bookmarks.store'

export const store = configureStore({
  reducer: {
    calculator: calculatorStore.reducer,
    bookmarks: bookmarksSlice.reducer,
  },
})

export type RootState = ReturnType<typeof store.getState>

const selectSelf = (state: RootState) => state

export const selectBookmarks = createSelector(
  selectSelf,
  (state) => state.bookmarks.items,
)

export const selectBookmarksCount = createSelector(
  selectBookmarks,
  (items) => items.length,
)
