import React, { useState } from 'react'
import './App.css'
import { Navbar } from './components/Navbar/Navbar'
import { Search } from './tv/Search'
import { HashRouter, Route, Switch } from 'react-router-dom'
import { Home } from './pages/Home'
import { Contact } from './pages/Contact'
import { NotFound } from './pages/NotFound'
import { withTitle } from './components/withTitle'
import { PreferencesContext } from './context/PreferencesContext'
import { Settings } from './settings/Settings'
import { SettingsContext } from './settings/SettingsContext'
import { Columns } from './settings/settings.models'
import { SettingsProvider } from './settings/SettingsProvider'
import { Provider } from 'react-redux'
import { store } from './store'

// type AppReturnType = ReturnType<typeof App>
// type AppParameters = Parameters<typeof App>
// type SearchProps = ComponentProps<typeof Search>

function App() {
  const [initialQuery, setInitialQuery] = useState('flash')

  return (
    <div>
      <PreferencesContext.Provider
        value={{ initialQuery, itemsPerPage: 10, setInitialQuery }}
      >
        <SettingsProvider columns={3}>
          <Provider store={store}>
            <HashRouter>
              <Navbar />
              <main className="container mt-3">
                <Switch>
                  <Route
                    exact
                    path="/"
                    component={withTitle(Home, 'Home Page')}
                  />
                  <Route path="/tv" component={withTitle(Search, 'Shows')} />
                  <Route
                    path="/contact"
                    component={withTitle(Contact, 'Contact us')}
                  />
                  <Route
                    path="/settings"
                    component={withTitle(Settings, 'Settings')}
                  />
                  <Route component={withTitle(NotFound, '404')} />
                </Switch>
              </main>
            </HashRouter>
          </Provider>
        </SettingsProvider>
      </PreferencesContext.Provider>
    </div>
  )
}

export default App
