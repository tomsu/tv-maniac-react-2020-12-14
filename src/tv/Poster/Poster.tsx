import React, { FC, ReactNode } from 'react'
import { Show } from '../tv.models'
import styles from './Poster.module.scss'

// type Props = {
//   show: Pick<Show, 'image' | 'name'>
// }
type Props = Pick<Show, 'image' | 'name'> & {
  onWatched?: (a: number, b: number) => string,
  header?: ReactNode
}

export const Poster: FC<Props> = ({ image, name, onWatched, children, header }) => {
  const imageUrl = image
    ? image.medium
    : 'https://source.unsplash.com/210x295/?tv'

  return (
    <>
      {header}
      <img className={styles.image} src={imageUrl} alt="" />
      {children}
      <h3 onClick={() => onWatched?.(1, 2)}>{name}</h3>
    </>
  )
}
