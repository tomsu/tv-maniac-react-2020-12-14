import Axios from 'axios'
import { Show, ShowResponse } from './tv.models'
import { Poster } from './Poster/Poster'
import { SearchForm } from '../components/SearchForm/SearchForm'
import { FC, useCallback, useContext, useEffect, useRef, useState } from 'react'
import { PreferencesContext } from '../context/PreferencesContext'
import { SettingsContext } from '../settings/SettingsContext'
import { Columns } from '../settings/settings.models'
import { useDispatch, useSelector } from 'react-redux'
import {
  bookmarksSlice,
  fetchBookmarks,
  saveBookmark,
} from '../bookmarks/bookmarks.store'
import { selectBookmarks } from '../store'
import { Badge, BadgeHeader, BigBadge } from '../components/Badge/Badge'

export const Search: FC = () => {
  const [shows, setShows] = useState<Show[]>([])
  const myRef = useRef<HTMLHeadingElement>(null)
  const { initialQuery } = useContext(PreferencesContext)
  const { columns } = useContext(SettingsContext)

  const dispatch = useDispatch()
  const bookmarks = useSelector(selectBookmarks) as Show[]

  useEffect(() => {
    dispatch(fetchBookmarks())
  }, [dispatch])

  useEffect(() => {
    search(initialQuery)
  }, [initialQuery])

  const search = useCallback((query: string) => {
    console.log(myRef.current?.classList)
    const apiUrl = `https://api.tvmaze.com/search/shows?q=${query}`
    Axios.get<ShowResponse[]>(apiUrl).then((response) =>
      setShows(response.data.map(({ show }) => show)),
    )
  }, [])

  return (
    <div className="row">
      <section className="col-3">
        <h2 className="h3" ref={myRef}>
          Bookmarks
        </h2>
        <BadgeHeader value={bookmarks.length}>
          {bookmarks.length}
          <small> items</small>
        </BadgeHeader>
        <div className="row">
          {bookmarks.map((show) => (
            <div key={show.id} className="col-6">
              <Poster {...show} />
            </div>
          ))}
        </div>
      </section>

      <section className="col">
        <h1 className="h3">Find a show</h1>

        <SearchForm initialValue="batman" onSearch={(query) => search(query)} />

        <div className="row">
          {shows.map((show) => (
            <div
              className={`${columnsToBootstrap(columns)} mb-4`}
              key={show.id}
            >
              <Poster
                {...show}
                onWatched={() => 'hello'}
                header={
                  <div className="d-flex">
                    <small className="mr-2">📼</small>
                    <h5>show</h5>
                  </div>
                }
              >
                {/*<BookmarkAdd item={show} />*/}
                <button
                  disabled={bookmarks.some(({ id }) => id === show.id)}
                  className="btn btn-sm btn-primary"
                  onClick={() => dispatch(saveBookmark(show))}
                >
                  save
                </button>
                <button
                  disabled={!bookmarks.some(({ id }) => id === show.id)}
                  className="btn btn-sm btn-danger"
                  onClick={() =>
                    dispatch(bookmarksSlice.actions.remove(show.id))
                  }
                >
                  remove
                </button>
              </Poster>
            </div>
          ))}
        </div>
      </section>
    </div>
  )
}

function columnsToBootstrap(value: Columns): string {
  const map: Record<Columns, number> = {
    1: 12,
    2: 6,
    3: 4,
    4: 3,
  }

  return `col-${map[value]}`
}
