import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Bookmark, BookmarkId } from './bookmarks.models'
import Axios from 'axios'

const initialState: { items: Bookmark[] } = {
  items: [],
}

export const saveBookmark = createAsyncThunk(
  'bookmarks/save',
  (item: Bookmark) => {
    return Axios.post('http://localhost:3000/bookmarks', item).then(
      (resp) => {
        // dispatch(updateFilteredBookmarks())
        return resp.data
      },
    )
  },
)

export const fetchBookmarks = createAsyncThunk('bookmarks/fetch', () => {
  return Axios.get('http://localhost:3000/bookmarks').then((resp) => resp.data)
})

export const bookmarksSlice = createSlice({
  name: 'bookmarks',
  initialState,
  reducers: {
    add(state, { payload }: PayloadAction<Bookmark>) {
      state.items.push(payload)
    },

    remove(state, { payload }: PayloadAction<BookmarkId>) {
      const index = state.items.findIndex(({ id }) => id === payload)
      if (index > -1) {
        state.items.splice(index, 1)
      }
      // return {
      //   items: state.items.filter(({ id }) => id !== payload),
      // }
    },

    clear(state) {
      state.items = []
      // state.items.length = 0
    },
  },
  extraReducers: {
    [saveBookmark.fulfilled.type]: (state, action: PayloadAction<Bookmark>) => {
      state.items.push(action.payload)
    },

    [fetchBookmarks.fulfilled.type]: (
      state,
      action: PayloadAction<Bookmark[]>,
    ) => {
      state.items = action.payload
    },
  },
})
