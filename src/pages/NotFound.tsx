import { FC } from 'react'

export const NotFound: FC = () => <h1>Uh oh! This page does not exist!</h1>
