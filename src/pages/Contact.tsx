import { FC } from 'react'

export const Contact: FC = () => (
  <>
    <h1>Contact Us</h1>
  </>
)

// const add = (a:number, b:number) => a + b

// const adder = (a:number) => (b:number) => a + b
// const add2 = adder(2)
// const add4 = adder(4)
//
// add2(5)
// add2(6)
// add2(9)
// add4(5)
// add4(6)
// add4(9)
