# Resources:

# React
- https://reactjs.org
- https://kentcdodds.com/blog
- https://overreacted.io

# Design system library
- https://blueprintjs.com
- https://material-ui.com
- https://www.primefaces.org/primereact

# React for Mobiles
- https://reactnative.dev

# React with TypeScript
- https://github.com/piotrwitek/react-redux-typescript-guide
- https://github.com/typescript-cheatsheets/react

# State management
- https://github.com/piotrwitek/typesafe-actions
- https://mobx.js.org/
- https://mobx-state-tree.js.org/

# General
- https://nx.dev/
- https://www.netlify.com
- https://github.com/gothinkster/realworld
- https://devchat.tv/
- https://www.cypress.io/

# Trener
- https://twitter.com/sulco
